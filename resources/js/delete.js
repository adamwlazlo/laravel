$(function () {
    $('.delete').click(function () {
        Swal.fire({
            title: "Czy na pewno usunąć?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Tak',
            cancelButtonText: 'Nie'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    method: "DELETE",
                    url: deleteUrl + $(this).data("id")
                    // data: { id: $(this).data("id")},
                }).done(function (data) {
                    console.log(data);
                    Swal.fire('OK!', data.message, data.status).then((result) => {
                        window.location.reload()
                    });
                }).fail(function (data) {
                    console.log(data);
                    Swal.fire('NIE OK!', data.responseJSON.message, data.responseJSON.status);
                });
            }
        });
    });
});
