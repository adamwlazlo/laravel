@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="">
                <div class="row justify-content-center">
                    <div class="col-6"><h2>Lista produktów</h2></div>
                    <div class="col-6">
                        <a class="float-right" href="{{route('products.create')}}">
                            <button type="button" class="btn btn-primary">Dodaj</button>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nazwa</th>
                        <th scope="col">Opis</th>
                        <th scope="col">Ilość</th>
                        <th scope="col">Cena</th>
                        <th scope="col">Akcja</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <th scope="row">{{$product->id}}</th>
                            <td>
                                <a class="float-right" href="{{route('products.show',$product->id)}}">
                                    {{$product->name}}
                                </a>
                               </td>
                            <td>{{$product->description}}</td>
                            <td>{{$product->amount}}</td>
                            <td>{{$product->price}}</td>
                            <td>
                                <button class="btn btn-info btn-sm">
                                    <a href="{{route('products.edit', $product->id)}}">Edytuj</a>
                                </button>
                            </td>
                            <td>
                                <button class="btn btn-danger btn-sm delete" data-id="{{$product->id}}">Usuń</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
                {{$products->links()}}
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    const deleteUrl = "{{ url('products') }}/";
@endsection
@section('javascript-files')
    <script src="{{ asset('js/delete.js') }}"></script>
@endsection
