<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            h2 { color: red;}
        </style>

    </head>
    <body class="antialiased">
       <div>
           <h2>{{ $title }}</h2>
           <p>{{ $paragraph }}</p>
       </div>

       <form method="post" action="example/1">
            @csrf
           <input type="text" name="var" value="10000">
           <input type="submit" value="send">
       </form>
       <form method="post" action="example/2">
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <input type="text" name="var" value="20000">
           <input type="submit" value="send">
       </form>
       <form method="POST" action="example/3">
           {{--           <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
           <input type="text" name="var" value="30000">
           @csrf
           @method('put')
           <input type="submit" value="send">
           <i>Mozna inne PUD, PACZ, DILIT</i>
       </form>

       <form method="POST" action="example/4">
           <input type="text" name="var" value="40000">
           @csrf
           @method('PUT')
           <input type="submit" value="send">
       </form>

       <form method="POST" action="example/5">
           <input type="text" name="var" value="50000">
           @csrf
           @method('delete')
{{--           <input type="hidden" name="_method" value="POST">--}}
           <input type="submit" value="send">
       </form>

    </body>
</html>
